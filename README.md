# Wercker serverless step

A [Wercker](http://wercker.com) [Serverless]( http://www.serverless.com) step to execute commands using the serverless cli. The version
of this step corresponds to the same Serverless version.

*You should have nodejs and npm installed.*

## Example Usage

Place this setup in your [wercker.yml](http://devcenter.wercker.com/articles/werckeryml/) file under the `build` section:

``` yaml
build:
  steps:
    - ajagnanan/serverless@1.20.0:
        task: "function deploy -a"
        region: us-east-1
        stage: dev
```

## Properties

### task
- type: string
- optional: false
- description: Task which should be run.
- example: `task: "function deploy -a"`

### region
- type: string
- optional: false
- description: Region configuration to run against.
- example: `region: us-east-1`

### stage
- type: string
- optional: false
- description: Stage configuration to run against.
- example: `stage: dev`
