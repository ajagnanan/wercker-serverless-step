#!/bin/bash

# return true if local npm package is installed at ./node_modules, else false
# example
# echo "gruntacular : $(npm_package_is_installed gruntacular)"
npm_package_is_installed() {
  # set to true initially
  local return_=true
  # set to false if not found
  ls node_modules | grep "$1" >/dev/null 2>&1 || { local return_=false; }
  # return value
  echo "$return_"
}

# First make sure serverless is installed
if ! type sls &> /dev/null ; then
    # Check if it is in repo
    if ! $(npm_package_is_installed sls) ; then
        info "sls not installed, trying to install it through npm"

        if ! type npm &> /dev/null ; then
            fail "npm not found, make sure you have npm or sls installed"
        else
            info "npm is available"
            debug "npm version: $(npm --version)"

            info "installing sls"
            sudo npm install -g serverless@1.20.0
            sls_command="sls"
            debug "serverless version: $(sls --version)"
        fi
    else
        info "sls is available locally"
        debug "sls version: $(./node_modules/serverless/bin/serverless --version)"
        sls_command="./node_modules/serverless/bin/serverless"
    fi
else
    info "serverless is available"
    debug "serverless version: $(sls --version)"
    sls_command="sls"
fi

# Parse some variable arguments
if [ -n "$WERCKER_SERVERLESS_REGION" ] ; then
    sls_command="$sls_command -r $WERCKER_SERVERLESS_REGION"
fi

if [ -n "$WERCKER_SERVERLESS_STAGE" ] ; then
    sls_command="$sls_command -s $WERCKER_SERVERLESS_STAGE"
fi

if [ -n "$WERCKER_SERVERLESS_TASK" ] ; then
    sls_command="$sls_command $WERCKER_SERVERLESS_TASK"
fi

debug "$sls_command"

set +e
$sls_command
result="$?"
set -e

if [[ $result -eq 0 ]]; then
  success "finished $sls_command"
elif [[ $result -eq 6 && "$WERCKER_SERVERLESS_FAIL_ON_WARNINGS" != 'true' ]]; then
  warn "serverless returned warnings, however fail-on-warnings is not true"
  success "finished $sls_command"
else
    warn "serverless exited with exit code: $result"
    fail "serverless failed"
fi
